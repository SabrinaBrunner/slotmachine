var Mediator = (function () {

    var channels = {};

    //
    // Subscribe to a channel
    //
    function subscribe(channel, fn, that) {
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({
            context: that,
            callback: fn
        });
    }

    //
    // Publish a channel (like triggering an event)
    //
    function publish(channel) {

        if (!channels[channel]) {
            return false;
        }
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0, l = channels[channel].length; i < l; i += 1) {
            var subscription = channels[channel][i];
            if (typeof subscription.context === undefined) {
                subscription.callback(args);
            } else {
                subscription.callback.call(subscription.context, args);
            }
        }
    }

    return {
        subscribe: subscribe,
        publish: publish
    }

})();

/*
console.log("...");


var BackendConnector = (function () {
    function send() {
        $.ajax({
            url: "http://fettblog.eu/fh/numbers.php?jsonp",
            dataType: 'jsonp',
            success: function (data) {
                Mediator.publish('datareceived',data);
            },
            error: function (err) {

            }
        });
    }

    Mediator.subscribe('inputGiven', send);

})();

$('html').on('mousedown', function () {
    Mediator.publish('inputGiven');
});

Mediator.subscribe('datareceived', function (data) {
    alert("Meine Daten sind hier " + data[0].slots);
});

*/