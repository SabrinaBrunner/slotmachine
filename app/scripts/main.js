$('html').on('keydown', function() {
	Mediator.publish('startGame');
});

$('html').on('mousedown', function() {
	Mediator.publish('startGame');
});

Mediator.subscribe('startGame', startGame);

var serverData;
var animationcnt = 0;
var score = 0;
var audioElement = new Audio("");
var loopCherry  = new Audio("");


function ClassForId(i){
	if(i == 0){
		return 'slot-cherry';
	}else if(i == 1){
		return 'slot-enemy';
	}else if(i == 2){
		return 'slot-vegetable';
	}else{
		return 'slot-star';
	}
}

function ClassForSlot(s){
	return ClassForId(serverData[0].slots[s-1]);
}

function AnimationEnd(){
	animationcnt--;
	if(animationcnt == 0){
		$('#coins').html(score);
		if(serverData[0].result != 0){
			audioElement.src = '../assets/smb2_bonus_chance_win.wav';
			audioElement.play();
		}
		else{
			audioElement.src = '../assets/smb2_bonus_chance_lose.wav';
			audioElement.play();
		}
	}
}

function startGame(){
	if(animationcnt == 0){
		loadData();
	}
}

function loadData(){
	$.ajax({
		url: "http://dragonquest.at/numbers.php?jsonp",
    dataType: 'jsonp',
    success: function (data) {
      Mediator.publish('datareceived',data);
			
    },
    error: function (err) {

    }
  });
}

Mediator.subscribe('datareceived', startAnimation);

function startAnimation(data){
	serverData = data;
	score += serverData[0].result;
	console.log(serverData[0].slots);
	audioElement.src = '../assets/smb2_bonus_chance_start.wav';
	audioElement.play();
	$('#slot1').addClass('animation1');
	$('#slot2').addClass('animation2');
	$('#slot3').addClass('animation3');
	animationcnt = 3;
}

$('#slot1').on('webkitAnimationEnd msAnimationEnd oAnimationend animationend' , function(e){
	$('#slot1').removeClass().addClass('slot').addClass(ClassForSlot(1));
	AnimationEnd();
});

$('#slot2').on('webkitAnimationEnd msAnimationEnd oAnimationend animationend' , function(e){
	$('#slot2').removeClass().addClass('slot').addClass(ClassForSlot(2));
	AnimationEnd();
});

$('#slot3').on('webkitAnimationEnd msAnimationEnd oAnimationend animationend' , function(e){
	$('#slot3').removeClass().addClass('slot').addClass(ClassForSlot(3));
	AnimationEnd();
});

